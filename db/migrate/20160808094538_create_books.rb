class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.belongs_to :user
      t.string :image_url
      t.string :author
      t.string :title
      t.string :publisher
      t.integer :year
      t.string :format
      t.string :size
      t.integer :pages
      t.string :language
      t.string :isbn
      t.string :quality
      t.string :series
      t.text :description

      t.timestamps null: false
    end
  end
end
