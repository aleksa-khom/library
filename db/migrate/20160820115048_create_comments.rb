class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.belongs_to :user
      t.belongs_to :book
      t.text :message

      t.timestamps null: false
    end
  end
end
