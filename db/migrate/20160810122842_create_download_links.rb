class CreateDownloadLinks < ActiveRecord::Migration
  def change
    create_table :download_links do |t|
      t.belongs_to :book
      t.string :url

      t.timestamps null: false
    end
  end
end
