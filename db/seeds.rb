['Новости', 'Информация', 'Наука и образование', 'Компьютеры и сети', 'Все краски мира', 'Цивилизация', 'Общество', 'Деньги', 'Человек', 'Досуг и хобби', 'Дом и семья', 'Литература', 'Разное', 'Подвал'].each do |item|
  Category.create!(name: item)
end
#
# ['Коммюнике', 'Вопросы и ответы', 'Жизнь сайта', 'Новости и творчество'].each do |item|
#   Category.create(name: item, parent: Category.find_by_name('Новости'))
# end
#
# ['Нормативно-справочная', 'Инфо-технологии','Энциклопедии', 'Справочники', 'Словари','Руководства', 'Самоучители'].each do |item|
#   Category.create(name: item, parent: Category.find_by_name('Информация'))
# end
#
# ['Вопросы образования', 'Точные','Технические', 'О живом', 'Земля и вселенная','О человеке', 'Научно-популярное'].each do |item|
#   Category.create(name: item, parent: Category.find_by_name('Наука и образование'))
# end
#
# ['Мануалы', 'Программинг','Сетевые технологии', 'ОС и БД', 'WEB-созидание','Железо', 'Компарт', 'Софт'].each do |item|
#   Category.create(name: item, parent: Category.find_by_name('Компьютеры и сети'))
# end
#
# ['Фото-видео', 'Дизайн и графика', 'Живопись и рисование', 'Архитектура','Звук-музыка'].each do |item|
#   Category.create(name: item, parent: Category.find_by_name('Все краски мира'))
# end
#
# ['Инжиниринг', 'Техника','Транспорт', 'Аппаратура', 'Строительство','Промышленность', 'Энергия', 'Профессии'].each do |item|
#   Category.create(name: item, parent: Category.find_by_name('Цивилизация'))
# end
#
# ['История', 'Военное дело', 'Социальное', 'Религия и эзотерика', 'Страны и народы','Языки', 'Искусство', 'Культура', 'Биографии и мемуары'].each do |item|
#   Category.create(name: item, parent: Category.find_by_name('Общество'))
# end
#
# ['Деньги'].each do |item|
#   Category.create(name: item, parent: Category.find_by_name('Деньги'))
# end
#
# ['Здоровье', 'Общение','Саморазвитие', 'Спорт', 'Школа выживания'].each do |item|
#   Category.create(name: item, parent: Category.find_by_name('Человек'))
# end
#
# ['Активный отдых', 'Игры','Коллекции', 'Рукоделие', 'Поделки','Прочие хобби'].each do |item|
#   Category.create(name: item, parent: Category.find_by_name('Досуг и хобби'))
# end
#
# ['Дом', 'Семья','Дети', 'Сделай сам', 'Кулинария','Растения', 'Животные'].each do |item|
#   Category.create(name: item, parent: Category.find_by_name('Дом и семья'))
# end
#
# ['Литературоведение', 'Публицистика','Проза', 'Поэзия и драматургия', 'Фантастика','Развлекательная', 'Историческая', 'Прочая'].each do |item|
#   Category.create(name: item, parent: Category.find_by_name('Литература'))
# end
#
# ['Журналы', 'Анонсы'].each do |item|
#   Category.create(name: item, parent: Category.find_by_name('Разное'))
# end
#
# Category.create(name: "Подвал", parent: Category.find_by_name('Подвал'))
#
#
#
# %w(librarian admin local member).each do |role|
#   Role.create!(name: role)
# end


