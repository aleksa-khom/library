Rails.application.routes.draw do

  devise_for :users
  root 'books#index'

  resources :books
  resources :download_links
  resources :comments, only: [:create, :destroy, :update]

  namespace :admin do
    resources :books, only: [:index, :destroy]
    resources :comments, only: [:index, :edit, :update, :destroy]
    resources :users, only: [:index, :edit, :update, :destroy]
  end
end
