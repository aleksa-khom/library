$(document).ready(function() {
    $('select').material_select();
    $('textarea#book_description').characterCounter();

    $.each($(':checkbox'), function(k, v) { var label = $('label[for="' + this.id + '"]'); $(this).insertBefore(label); });

    $('body').on('click', '#js-add-url', function(e){
        e.preventDefault();
        if ($('#download-url').length) {
            $('#download-url').remove()
        } else {
            $('.js-add-link').append('<div class="row" id="download-url">' +
                  '<div class="col s9"><input id="download-link" class="string required" type="text" name="book[url]"><span class="error"></span></div>' +
                  '<div class="col s3"><button id="js-add-link" class="waves-effect waves-light btn indigo lighten-2">Добавить</button></div>' +
              '</div>')
        }
    });

    $("body").on('click', '#js-remove-url', function(e){
        e.preventDefault();
        id = $(this).data('id');
        $.ajax({
            url: "/download_links/" + id,
            type: "DELETE",
            dataType: "text"
        }).success(function (data) {
            $("tr#" + id).remove();
        }).fail(function (d, c) {
            if (d["status"] == '403'){
                alert("Это действие запрещено!");
            } else {
                alert("Произошла ошибка. Повторите действие позже. " + c);
            }
        });
    });

    $('body').on('click', 'button#js-add-link', function(e){
        e.preventDefault();
        var url = $('#download-link').val();
        var book_id = $('#book-id').val();
        if (url.length == 0 ) {
            alert('Введите, пожалуйста, ссылку')
        } else {
            $.ajax({
                url: "/download_links",
                type: "POST",
                dataType: "json",
                data: {url: url, book_id: book_id}
            }).success(function (data) {
                if (data['success'] === true) {
                    $('#download-links').html(data['html']);
                } else {
                    $('.error').html(data['errors']);
                }
            }).fail(function (d, c) {
                if (d["status"] == '403'){
                    alert("Это действие запрещено!");
                } else {
                    alert("Произошла ошибка. Повторите действие позже. " + c);
                }
            });
        }
    });

    $('p.expander').expander({
        slicePoint: 400,
        expandEffect: 'fadeIn',
        collapseEffect: 'fadeOut',
        userCollapseText: 'Свернуть',
        expandText: 'Читать дальше',
        preserveWords: true
    });
});
