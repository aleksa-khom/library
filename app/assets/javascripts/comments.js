$(document).ready(function(){
    $('#js-create-comment').click(function(e){
        e.preventDefault();
        var message = $('textarea#comment_message').val();
        if (message.length < 5) {
            alert('Введите текст!')
        } else if(message.length > 750) {
            alert('Комментарий слишком длинный!')
        } else {
            $.ajax({
                url: "/comments",
                type: "POST",
                dataType: "json",
                data: {
                    comment: {
                        message: message,
                        book_id: $('input#comment_book_id').val()
                    }
                }
            }).success(function (data) {
                $('.last-comments').append(data.html);
                $('textarea#comment_message').val(' ');
            }).fail(function (d, c) {
                if (d["status"] == '403') {
                    alert("Это действие запрещено!");
                } else {
                    alert("Произошла ошибка. Повторите действие позже. " + c);
                }
            });
        }
    });

    jQuery(".best_in_place").best_in_place();
});
