module BooksHelper
  def book_attrs(book, long=true)
    short_list = %w(author title year format size quality)
    long_list = short_list + %w(publisher pages language series isbn)
    attrs_list = long ? long_list : short_list
    html = ''
    attrs_list.each do |item|
      html += content_tag :span do
        content_tag(:strong, book_attr(item)) + ': ' + book.send(item).to_s + tag(:br)
      end if book.send(item).present?
    end
    html.html_safe
  end

  def book_attr(attr)
    I18n.t("activerecord.attributes.book.#{attr}")
  end

  def has_errors?(attr)
    @book.errors.messages[attr].blank?
  end

  def link_to_download(link)
    content_tag :a, href: link.url do
      "Скачать с #{URI.parse(link.url).host}"
    end
  end

  def book_cover(book)
    book.image_url.blank? ? 'default_cover.png' : book.image_url
  end
end
