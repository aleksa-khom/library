class Category < ActiveRecord::Base
  has_closure_tree

  has_and_belongs_to_many :books
  scope :by_name, -> { leaves.order(name: :asc) }
end
