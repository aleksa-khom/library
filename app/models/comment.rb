class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :book

  def comments_form
    "#{message} <br /> <span class='edit_link'> Редактировать </span>".html_safe
  end
end
