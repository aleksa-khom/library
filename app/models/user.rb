class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :username, presence: true, uniqueness: true
  has_many :books
  has_many :comments

  after_create :assign_role

  enum role: [:member, :local, :librarian, :admin]

  def has_role?(role)
    self.role == role
  end

  def owner?(resource)
    resource.user_id == self.id
  end

  def admin?
    role == 'admin'
  end

  private

  def assign_role
    self.member!
  end
end
