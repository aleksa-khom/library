class Book < ActiveRecord::Base

  has_and_belongs_to_many :categories
  has_many :download_links
  has_many :tasks
  accepts_nested_attributes_for :download_links

  validates :title, :author, :size, :pages, :format, :language, :categories, presence: true
  validates_length_of :description, maximum: 800, minimum: 10

  scope :by_category, -> (category) { joins(:categories).where('categories.name = ?', category) }

  belongs_to :user
  has_many :comments, dependent: :destroy
end
