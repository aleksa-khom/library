class DownloadLink < ActiveRecord::Base
  belongs_to :book

  validates :url, url: true
end
