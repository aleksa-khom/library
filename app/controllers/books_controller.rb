class BooksController < ApplicationController
  before_action :set_book, only: [:edit, :update, :show]
  before_action :authenticate_user!, only: [:new, :create, :edit, :update]

  def index
    @books = params[:category].present? ? Book.by_category(params[:category]).page(params[:page]) : Book.all.page(params[:page])
  end

  def new
    authorize Book
    @book = Book.new
    @book.download_links.build
  end

  def create
    authorize Book
    @book = Book.new(book_params)
    @book.user = current_user
    if @book.save
      redirect_to book_path(@book)
    else
      render 'new'
    end
  end

  def edit
    authorize @book
  end

  def update
    authorize @book
    if @book.save(book_params)
      redirect_to book_path(@book)
    else
      render 'edit'
    end
  end

  def show
  end

  private

  def book_params
    params.require(:book).permit(
        :title,
        :year,
        :publisher,
        :author,
        :format,
        :language,
        :image_url,
        :pages,
        :size,
        :isbn,
        :quality,
        :description,
        :series,
        category_ids: [],
        download_links_attributes: [:id, :_destroy, :url],
    )
  end

  def set_book
    @book = Book.find(params[:id])
  end
end
