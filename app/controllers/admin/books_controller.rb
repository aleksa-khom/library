class Admin::BooksController < Admin::ApplicationController
  before_action :set_book, only: [:edit, :update, :show, :destroy]

  def index
    @books = Book.all.page(params[:page])
  end

   def destroy
    @book.destroy
    redirect_to :back
  end

  private

  def book_params
    params.require(:book).permit(
        :title,
        :year,
        :publisher,
        :author,
        :format,
        :language,
        :image_url,
        :pages,
        :size,
        :isbn,
        :quality,
        :description,
        :series,
        category_ids: [],
        download_links_attributes: [:id, :_destroy, :url],
    )
  end

  def set_book
    @book = Book.find(params[:id])
  end
end
