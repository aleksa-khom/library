class Admin::CommentsController < Admin::ApplicationController
  before_action :set_comment, only: [:edit, :update, :destroy]

  def index
    @comments = Comment.all.page(params[:page])
  end

  def edit
  end

  def update
    if @comment.update(comment_params)
      redirect_to admin_comments_path, notice: 'Комментарий успешно отредактирован.'
    else
      render 'edit'
    end
  end

  def destroy
    @comment.destroy
    redirect_to :back
  end

  private

  def comment_params
    params.require(:comment).permit(:message)
  end

  def set_comment
    @comment = Comment.find(params[:id])
  end
end
