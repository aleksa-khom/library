class Admin::UsersController < Admin::ApplicationController
  before_action :set_user, only: [:edit, :update, :show, :destroy]

  def index
    @users = User.all.page(params[:page])
  end

  def edit
  end

  def update
    if @user.update(user_params)
      redirect_to admin_users_path(@user)
    else
      render 'edit'
    end
  end

  def destroy
    @user.destroy
    redirect_to :back
  end

  private

  def user_params
    params.require(:user).permit(params.require(:user).permit(:role))
  end

  def set_user
    @user = User.find(params[:id])
  end
end
