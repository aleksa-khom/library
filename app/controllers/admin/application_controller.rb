class Admin::ApplicationController < ActionController::Base
  layout 'admin'
  include Pundit
  before_action :authenticate_user!

  protect_from_forgery with: :exception

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
  end

  private

  def user_not_authorized
    flash[:alert] = "Это действие запрещено!"
    redirect_to(request.referrer || root_path)
  end
end
