class DownloadLinksController < ApplicationController
  def destroy
    @download_link = DownloadLink.find(params[:id])
    respond_to do |format|
      if @download_link.destroy
        format.js { render status: 200, json: { message: 'Success' } }
      else
        format.js { render status: 403, json: { message: 'Forbidden' } }
      end
    end
  end

  def create
    @book = Book.find(params[:book_id])
    @link = DownloadLink.new(url: params[:url], book_id: @book.id)
    respond_to do |format|
      if @link.save
        format.js { render json: { success: true, html: render_to_string(partial: 'books/download_links') } }
      else
        format.js { render json: { success: false, errors: @link.errors.full_messages.join(' ') } }
      end
    end
  end
end
