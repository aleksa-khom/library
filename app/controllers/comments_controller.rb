class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    authorize Comment
    @comment = Comment.new(comment_params.merge(user_id: current_user.id))
    respond_to do |format|
      if @comment.save
        format.js { render json: { html: render_to_string(partial: 'comments/comment', locals: { comment: @comment }) } }
      else
        format.js { render json: { errors: @comment.errors.full_messages.join(' ') } }
      end
    end
  end

  def update
    @comment = Comment.find params[:id]

    respond_to do |format|
      if @comment.update_attributes(comment_params)
        format.json { respond_with_bip(@comment) }
      else
        format.json { respond_with_bip(@comment) }
      end
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    authorize @comment
    @comment.destroy
    redirect_to :back
  end

  private

  def comment_params
    params.require(:comment).permit(:book_id, :user_id, :message)
  end
end
