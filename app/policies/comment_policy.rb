class CommentPolicy < ApplicationPolicy
  def create?
    user.present?
  end

  def edit?
    create? && (user.owner?(@record) || user.has_role?(:admin))
  end

  def update?
    edit?
  end

  def destroy?
    edit?
  end
end
