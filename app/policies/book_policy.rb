class BookPolicy < ApplicationPolicy
  def index?
    true
  end

  def show
    index?
  end

  def new?
    user && user.has_role?('admin')
  end

  def create?
    user && user.has_role?('admin')
  end

  def edit?
    user_can_edit?
  end

  def update?
    edit?
  end

  private

  def user_can_edit?
    user && (user.has_role?('admin') || user.owner?(@book))
  end
end
